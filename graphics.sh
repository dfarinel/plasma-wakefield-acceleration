sh plot2d.sh | gnuplot
NOW=$(date +"%m-%d-%h:%s")
NAME1="PWFA_gauss_packet_test_"$NOW
#NAME2="contour_"$NOW
convert -delay 25 -loop 0 field*.png $NAME1.gif
#sh 3dplot.sh | gnuplot
#convert -delay 25 -loop 0 contour*.png $NAME2.gif
mv *.gif /data/users/lauc2/website/gifs/
rm *png
