subroutine print(e,b,x,u,v,pden,nden)

  use param , only :m, dx, dt, gam0, n0

  implicit none
  real(8), intent(in) :: pden(:), nden(:)
  real(8), intent(in) :: e(:,:), b(:,:)
  real(8), intent(in) :: u(:,:,:), x(:,:,:), v(:,:,:)

  real(8) :: gamt(1:2)  
  integer :: k, i
  

  open(13, file='field.dat')
  open(20, file='positive.dat')
  open(21, file='negative.dat')  
  
  do i=1,m-1
!!! grid information !!!
     write(13,'(8e15.7)')e(i,1),e(i,2),e(i,3),b(i,1),b(i,2),b(i,3),pden(i),nden(i) 
  end do

  do k=1,n0
!!! phase space > phase.dat !!!
        gamt(1) = sqrt(1.0 + dot_product(u(k,1:3,1),u(k,1:3,1)))
        gamt(2) = sqrt(1.0 + dot_product(u(k,1:3,2),u(k,1:3,2)))
     write(20,'(9e15.7)') x(k,1,1), x(k,2,1), x(k,3,1), v(k,1,1), v(k,2,1), v(k,3,1), u(k,1,1), u(k,2,1), u(k,3,1)
     write(21,'(9e15.7)') x(k,1,2), x(k,2,2), x(k,3,2), v(k,1,2), v(k,2,2), v(k,3,2), u(k,1,2), u(k,2,2), u(k,3,2)
  end do
  
  close(13)
  close(20)
  close(21)  

    
end subroutine print
