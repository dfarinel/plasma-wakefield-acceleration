subroutine density(x,v,e,cur,pden,nden)

  use param , only : m, q, cc, n0, g0, gp
  
  implicit none
  
  real(8), intent(in) :: x(:,:,:), v(:,:,:)
  real(8), intent(out) :: e(:,:), cur(:,:)
  real(8), intent(out) :: nden(:),pden(:)

  integer :: s, k, i  
  real(8) :: xh, eb
  real(8) :: distp, distm, et(1:m), den(1:m), dentmp(1:m), curtmp(1:m,1:3)


!!! initialize !!!
  den(1:m)     = 0.0d0
  cur(1:m,2:3) = 0.0d0

  !write(1021,*) cc

  do s=1,2
!!$omp parallel private(dentmp,curtmp)
     dentmp = 0.0
     curtmp(1:m,1:3) = 0.0
!!$omp do private(k,i,distp,distm,xh)
     do k=1,n0
!!!!!!!!!!! charge density !!!!!!!!!!!!!
        i = int(x(k,1,s))
        distp = x(k,1,s) - real(i)
        distm = 1.0 - distp
        
        dentmp(i) = dentmp(i) + q(s)*distm
        dentmp(gp(i)) = dentmp(gp(i)) + q(s)*distp
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
!!!!!!!!!!!! current density !!!!!!!!!!!!!
        xh = x(k,1,s) - 0.5*v(k,1,s)
        i = int(xh)
        distp = xh - real(i)
        distm = 1.0 - distp
         
        curtmp(g0(i),2:3) = curtmp(g0(i),2:3) + cc*q(s)*v(k,2:3,s)*distm
        curtmp(gp(i),2:3) = curtmp(gp(i),2:3) + cc*q(s)*v(k,2:3,s)*distp

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
     end do
!!$omp end do
!!$omp critical
     do i=0,m-1
        den(i)=den(i)+dentmp(i)
        cur(i,2:3)=cur(i,2:3)+curtmp(i,2:3)      
     end do
!!$omp end critical
!!$omp end parallel
     if (s == 1) then
        pden(1:m)=abs(den(1:m)/sum(den(1:m)))
     else if (s == 2) then
        nden(1:m)=abs((den(1:m)-pden(1:m))/sum(den(1:m)-pden(1:m)))
     endif
  end do

  et(1) = 0.0d0
  !et is electric field transverse!!

!!$omp parallel do private(i)
  do i=1,m-2
     et(gp(i)) = et(g0(i)) + cc*den(g0(i))
  end do
!!$omp end parallel do

  eb = sum(et(1:m-1))/(m-1) 

!!$omp parallel do private(i)
  do i=1,m-1
     e(g0(i),1) = 0.5*(et(g0(i)) + et(gp(i))) - eb
  end do
!!$omp end parallel do

end subroutine density
