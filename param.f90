module param
  
  implicit none  


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!                  setting parameters              !!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  real(8) :: field_amplitude,step_size,sim_length,sim_duration,bulk_unit_of_mag_field,&
                    pulselength
  real(8),dimension(2) :: bulk_velocity,thermal_velocity,charge,mass_ratios
  integer :: wave_numbers,particles_per_grid,monitor_e,monitor_h,display_t,icase,irun

  real(8) :: b0(1:3),e0(1:3),ub(1:2),uth(1:2),dt,ss,ts,sigma,q(1:2),mass(1:2),&
             pulse_length
  integer :: wavenum,ppg,monse,mone,monsh,monh,dispst,dispt,casenum,runnum
  logical :: file_exist

  namelist /input_parameters/ icase,field_amplitude,wave_numbers,&
            bulk_velocity,thermal_velocity,charge,mass_ratios,&
            step_size,sim_length,sim_duration,particles_per_grid,&
            bulk_unit_of_mag_field,monitor_e,monitor_h,display_t,&
            pulse_length,irun

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !! grid size
  real(8) :: dx
  
  !! total particle number ("p" or "e")
  integer :: n0    

  !! grid number
  integer :: m

  !! time iteration
  integer :: time

  real(8) :: gam0
  real(8) :: cc, econ
  real(8) :: pi
  real(8) :: vb(1:2)
  real(8) :: gamb(1:2)
  real(8) :: vth(1:2)
  real(8) :: gamth(1:2)
  real(8) :: dl
  integer :: i
  integer, allocatable :: gp(:),g0(:),gm(:)

  contains

    subroutine initial_cal

      inquire(file='pwfa.in',exist=file_exist)
      if (file_exist) then
          open(55,file='pwfa.in',status='old')
          read(55,nml=input_parameters)
          close(55)
      else
          write(*,*)'Cannot find file gtc.in !!!'
          stop
      endif

      runnum = irun 
      casenum = icase
      b0(1:3) = (/ 0.0d0, 0.0d0, field_amplitude /)
      e0(1:3) = (/ 0.0d0, field_amplitude,0.0d0 /)
      wavenum = wave_numbers
      pulselength = 0.5*pulse_length

      ub(1:2) = (/ bulk_velocity(1),bulk_velocity(2) /)
      uth(1:2) = (/ thermal_velocity(1),thermal_velocity(2) /)

      dt = step_size
      ss = sim_length
      ts = sim_duration
      ppg = particles_per_grid

      sigma = bulk_unit_of_mag_field
      q(1:2) = (/ charge(1),charge(2) /)
      mass(1:2) = (/ mass_ratios(1),mass_ratios(2) /)

      monse = monitor_e
      mone = monitor_e
      monsh = monitor_h
      monh = monitor_h
      dispst = display_t
      dispt = display_t

      pi = 4.0*atan(1.0)
      cc = dt/(2.0*sigma*ppg)
      econ = sigma*ppg

      dx = dt      
      gamb(1) = sqrt(1.0 + ub(1)**2)
      gamb(2) = sqrt(1.0 + ub(2)**2)
      vb(1) = ub(1)/gamb(1)
      vb(2) = ub(2)/gamb(2)
      gam0 = gamb(2)

      gamth(1) = sqrt(1.0 + uth(1)**2)
      gamth(2) = sqrt(1.0 + uth(2)**2)
      vth(1) = uth(1)/gamth(1)
      vth(2) = uth(2)/gamth(2)

      dl = sqrt(2.0*(gamth(2)-1.0))*sigma/dx

      m = nint(ss/dx)
      n0 = int((m-1)*ppg)
      time = nint(ts/dt)

      monse = int(real(time)/real(mone))
      if(monse==0) monse=1
      monsh = int(real(time)/real(monh))
      if(monsh==0) monsh=1
      dispst = int(real(time)/real(dispt))
      if(dispst==0) dispst=1


      allocate(gp(0:m+1))
      allocate(g0(0:m+1))
      allocate(gm(0:m+1))

      do i=0,m+1
         gp(i) = i+1
         g0(i) = i
         gm(i) = i-1
         if(gp(i)>=m) gp(i) = gp(i)-m+1
         if(g0(i)>=m) g0(i) = g0(i)-m+1
         if(g0(i)<=0) g0(i) = g0(i)+m-1
         if(gm(i)<=0) gm(i) = gm(i)+m-1
      end do

    end subroutine initial_cal

end module param
