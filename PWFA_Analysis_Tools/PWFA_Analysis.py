# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

def pwfa_dat_name(i,stepsize):
    stepnumname=str((i*stepsize))
    add=7-len(stepnumname);
    stepnumname=("0"*add)+stepnumname
    return stepnumname

def read_pwfa_files(laststep,stepsize,filedirectory,file_a0_num,filedirectory_finish,filedesc,filefirstletter,colnum,file_extra_name=""):

    # Pack the data files into something more python friendly for later usage
    import cPickle
    import time

    now=time.clock()
    print "Start time=",now

    totalnum=[]
    for i in xrange(len(laststep)):
        totalnum.append((laststep[i]/stepsize[i])+1)

    for h in xrange(len(file_a0_num)):
        for k in xrange(2):
            data=[]
            print "Now at: ", str((h+1)+(len(file_a0_num)*(k))),"/",str(len(file_a0_num)*2)

            for i in xrange(totalnum[h]):
                stepnumname=pwfa_dat_name(i,stepsize[h])
                file_to_read=filedirectory+str(file_a0_num[h])+file_extra_name+"/"+filefirstletter[k]+stepnumname+".dat"
                print file_to_read
                tempfile = open(file_to_read,"r") 
                lines = tempfile.readlines()
                tempfile.close
                temp1=[]
                for j in xrange(colnum[k]):
                    temp2=[]
                    for line in lines:
                        temp2.append(float(line.strip().split()[j]))
                    temp1.append(temp2)
                data.append(temp1) #this way goes by time, then adding in next column
  #the data is in the form data[column][time][position]

  #print "Almost done!!"

            yay = open(filedirectory_finish+filefirstletter[k]+"_"+filedesc+"_"+str(file_a0_num[h])+".pckl","w")
            cPickle.dump([filedirectory,filefirstletter[k],colnum,laststep,stepsize,totalnum[h],data],yay)
            yay.close()
    #data is now stored in pickle file as the above indicates..

    later=time.clock()
    timecost=later-now
    print "End time=", later
    print "Congratulations, you're", timecost/60.0, "minutes older than when this run started!"
  
    
def a0_and_pondf_from_py(filedir,filedesc,filevar,look_for_pondf="yes"):
    #ex: a0_from_py("/share/dragon/lauc2/data/pwfa/mass-ratios/","mass",[1,2,5,10])
    #returns [max(a0),average(a0)]
    import numpy
    import cPickle
    import time
    now=time.clock()
    print "Start time=",now
    filefirstlet="n"
    maxpondfpertime=[]
    maxpypertime=[]
    maxpondf=[]
    maxpy=[]
    avgpy=[]
    if look_for_pondf=='yes':
      for i in xrange(len(filevar)):
        pcklfile=filedir+filefirstlet+"_"+filedesc+"_"+str(filevar[i])+".pckl"
        print pcklfile
        [filedirectory,filefirstletter,colnum,laststep,stepsize,totalnum,data] = cPickle.load(open(pcklfile))
        temp1=[]
        temp2=[]
        for j in xrange(len(data[2][:])):    
            temp1.append(max(data[5][j][:]))
            temp2.append(max(data[2][j][:]))
        maxpondfpertime.append(temp1)
        maxpypertime.append(temp2)
        maxpondf.append(max(temp1))
        maxpy.append(max(temp2))
        avgpy.append(numpy.mean(temp2))
    else:
      for i in xrange(len(filevar)):
        pcklfile=filedir+filefirstlet+"_"+filedesc+"_"+str(filevar[i])+".pckl"
        print pcklfile
        [filedirectory,filefirstletter,colnum,laststep,stepsize,totalnum,data] = cPickle.load(open(pcklfile))
        temp1=[]
        temp2=[]
        for j in xrange(len(data[2][:])):    
            temp2.append(max(data[2][j][:]))
        maxpypertime.append(temp2)
        maxpy.append(max(temp2))
        avgpy.append(numpy.mean(temp2))
    later=time.clock()
    timecost=later-now
    print "End time=", later
    print "Congratulations, you're", timecost/60.0, "minutes older than when this run started!"
    return maxpy,avgpy,maxpondf

def deriv(array):
    length=len(array)
    magnitude=sum(array)/float(length)
    result=[]
    for i in xrange(length-1):
        result.append((array[i+1]-array[i])/magnitude)
    return result

def zeeros(array):
    import numpy
    length=len(array)
    result=[]
    for i in xrange(length-1):
        temp=numpy.sign(array[i+1])*numpy.sign(array[i])
        if temp<= 0:
            result.append(i)
    return result

def max_energy(filedir,filevar,output="less"):
    #ex: a0_from_py("/share/dragon/lauc2/data/pwfa/mass-ratios/",[1,2,5,10])
    #returns [max_energy,time_to_max_energy,maxenergy_per_time,time_in_sim]
    import cPickle
    import time
    now=time.clock()
    print "Start time=",now
    maxenergy_per_time=[]
    time_in_sim=[]
    max_energy=[]
    time_to_max_energy=[]
    
    for i in xrange(len(filevar)):
        datfile=filedir+str(filevar[i])+"/energy.dat"
        print datfile
        data_file = open(datfile,'r')
        lines = data_file.readlines()
        data_file.close()
        temp1=[]
        temp2=[]
        for line in lines:
            temp1.append(float(line.strip().split()[0]))
            temp2.append(float(line.strip().split()[5]))
        time_in_sim.append(temp1)
        maxenergy_per_time.append(temp2)
        
    for i in xrange(len(filevar)):
        temp3=zeeros(deriv(maxenergy_per_time[i]))
        max_energy.append(maxenergy_per_time[i][temp3[0]])
        time_to_max_energy.append(time_in_sim[i][temp3[0]])
        
    later=time.clock()
    timecost=later-now
    print "End time=", later
    print "Congratulations, you're", timecost/60.0, "minutes older than when this run started!"
    if output=="more":
        return max_energy,time_to_max_energy,maxenergy_per_time,time_in_sim
    if output=="less":
        return max_energy,time_to_max_energy

def make_log_scaling_plot(x,y,xvar,yvar,plottitle,rounding_digits=2,criticalpts=[],makefig="true",return_all="false",blackwhite='true'):
    import numpy
    import matplotlib.pyplot as plt
    xlogfit=[]
    ylogfit=[]
    xlog=[]
    ylog=[]
    slope=[]
    const=[]
    x_title="Log10("+xvar+")"
    y_title="Log10("+yvar+")"
    
    if len(criticalpts)==0:
        temp_xlog=[]
        temp_ylog=[]
        for i in xrange(len(x)):
            temp_xlog.append(numpy.log10(x[i]))
            temp_ylog.append(numpy.log10(y[i]))
        xlog.append(temp_xlog)
        ylog.append(temp_ylog)
        linear_fit,res,_,_,_=numpy.polyfit(xlog[0],ylog[0],1,full=True)
        slope.append(linear_fit[0])
        const.append(linear_fit[1])
    if len(criticalpts)==1:
        x_new1=x[0:criticalpts[0]]
        x_new2=x[criticalpts[0]:]
        y_new1=y[0:criticalpts[0]]
        y_new2=y[criticalpts[0]:]
        temp_xlog=[]
        temp_ylog=[]
        for i in xrange(len(x_new1)):
            temp_xlog.append(numpy.log10(x_new1[i]))
            temp_ylog.append(numpy.log10(y_new1[i]))
        xlog.append(temp_xlog)
        ylog.append(temp_ylog)
        temp_xlog=[]
        temp_ylog=[]
        for i in xrange(len(x_new2)):
            temp_xlog.append(numpy.log10(x_new2[i]))
            temp_ylog.append(numpy.log10(y_new2[i]))
        xlog.append(temp_xlog)
        ylog.append(temp_ylog)
        for i in xrange(2):
            linear_fit,res,_,_,_=numpy.polyfit(xlog[i],ylog[i],1,full=True)
            slope.append(linear_fit[0])
            const.append(linear_fit[1])
    if len(criticalpts)==2:
        x_new1=x[:criticalpts[0]]
        x_new2=x[criticalpts[0]:criticalpts[1]]
        x_new3=x[criticalpts[1]:]
        y_new1=y[0:criticalpts[0]]
        y_new2=y[criticalpts[0]:criticalpts[1]]
        y_new3=y[criticalpts[1]:]
        temp_xlog=[]
        temp_ylog=[]
        for i in xrange(len(x_new1)):
            temp_xlog.append(numpy.log10(x_new1[i]))
            temp_ylog.append(numpy.log10(y_new1[i]))
        xlog.append(temp_xlog)
        ylog.append(temp_ylog)
        temp_xlog=[]
        temp_ylog=[]
        for i in xrange(len(x_new2)):
            temp_xlog.append(numpy.log10(x_new2[i]))
            temp_ylog.append(numpy.log10(y_new2[i]))
        xlog.append(temp_xlog)
        ylog.append(temp_ylog)
        temp_xlog=[]
        temp_ylog=[]
        for i in xrange(len(x_new3)):
            temp_xlog.append(numpy.log10(x_new3[i]))
            temp_ylog.append(numpy.log10(y_new3[i]))
        xlog.append(temp_xlog)
        ylog.append(temp_ylog)
        
        linear_fit,res,_,_,_=numpy.polyfit(xlog[1],ylog[1],1,full=True)
        slope.append(linear_fit[0])
        const.append(linear_fit[1])
    
    if makefig=="true":
        plt.figure(figsize=(7,5))
    for i in xrange(len(slope)):
        temp_xlogfit=[]
        temp_ylogfit=[]
        for j in xrange(len(x)):
            temp1=(j*(2000.0)/(len(x)-1))-1000.0
            temp2=(slope[i]*temp1)+const[i]
            temp_xlogfit.append(temp1)
            temp_ylogfit.append(temp2)
        xlogfit.append(temp_xlogfit)
        ylogfit.append(temp_ylogfit)
        
    xtemp_in=0
    xtemp_out=3
    if len(xlog[2])==0:
        xtemp_out=2
    if len(xlog[0])==0:
        xtemp_in=2
    xmin=min(min(xlog[xtemp_in:xtemp_out]))-((max(max(xlog[xtemp_in:xtemp_out]))-min(min(xlog[xtemp_in:xtemp_out])))*0.25)
    xmax=max(max(xlog[xtemp_in:xtemp_out]))+((max(max(xlog[xtemp_in:xtemp_out]))-min(min(xlog[xtemp_in:xtemp_out])))*0.25)
    ymin=min(min(ylog[xtemp_in:xtemp_out]))-((max(max(ylog[xtemp_in:xtemp_out]))-min(min(ylog[xtemp_in:xtemp_out])))*0.25)
    ymax=max(max(ylog[xtemp_in:xtemp_out]))+((max(max(ylog[xtemp_in:xtemp_out]))-min(min(ylog[xtemp_in:xtemp_out])))*0.25)

    if blackwhite=='true':
        plotmarker1='wo'
        plotmarker2='ko'
        plotfitline='k--'
    if len(xlog[0])!=0:
        plt.plot(xlog[0],ylog[0],plotmarker2,markersize=9,markeredgewidth=2)
    if len(xlog[2])!=0:
        plt.plot(xlog[2],ylog[2],plotmarker2,markersize=9,markeredgewidth=2)
    plt.plot(xlog[1],ylog[1],plotmarker1,markersize=9,markeredgewidth=2)
    plt.plot(xlogfit[0],ylogfit[0],plotfitline,markersize=9,markeredgewidth=2)
    titleamend=str(round(slope[0],rounding_digits))
    #titleamend.append(str(round(slope[0],rounding_digits)))
    #for i in xrange(len(slope)-1):
        #titleamend.append(" ,"+str(round(slope[i+1],rounding_digits)))
        #titleamend=titleamend+" ,"+str(round(slope[i+1],rounding_digits))
    plt.xlim([xmin,xmax])
    plt.ylim([ymin,ymax])
    plt.title(plottitle+", slope ~ "+titleamend)
    plt.xlabel(x_title)
    plt.ylabel(y_title)
    if return_all=="true":
        return slope, const, res
    else:
        return slope

def efield_from_pickle(filedir,filedesc,filevar,range_to_look_at=0.2):
    #ex: efield_from_pickle("/share/dragon/lauc2/data/pwfa/mass-ratios/","mass",[1,2,5,10],0.2)
    #returns [max(a0),average(a0)]
    import cPickle
    import time
    import numpy
    now=time.clock()
    print "Start time=",now
    filefirstlet="f"
    ex_pertime=[]
    time_in_sim=[]
    ey_pertime=[]
    max_ex=[]
    max_ey=[]
    
    rng=[]
    avg_ex=[]
    avg_ey=[]
    for i in xrange(len(filevar)):
        pcklfile=filedir+filefirstlet+"_"+filedesc+"_"+str(filevar[i])+".pckl"
        print pcklfile
        [filedirectory,filefirstletter,colnum,laststep,stepsize,totalnum,data] = cPickle.load(open(pcklfile))
        temp0=[]
        temp1=[]
        temp2=[]
        print len(data[2][:])
        for j in xrange(len(data[:][2])):
            temp4=len(data[j][0][:])/10.0
            temp0.append(data[j][0][:])
            temp1.append(data[j][1][:])
            temp2.append(data[j][2][:])
        ex_pertime.append(temp1)
        ey_pertime.append(temp2)
        time_in_sim.append((temp0))
    for i in xrange(len(filevar)):
        temp5=[]
        temp6=[]
        temp7=[]
        for j in xrange(len(ex_pertime[i])):
            range_of_search=int(len(ex_pertime[i][j])*range_to_look_at*0.5)
            temp_max_ey=max(ey_pertime[i][j])
            center_pt=ey_pertime[i][j].index(temp_max_ey)
            leftside=center_pt-range_of_search
            rightside=center_pt+range_of_search
            if leftside < 0:
                leftside=0
            elif leftside > len(ex_pertime[i][j]):
                leftside=len(ex_pertime[i][j])-1
            elif rightside < 0:
                rightside=0
            elif rightside > len(ex_pertime[i][j]):
                rightside=len(ex_pertime[i][j])-1
            temp_max_ex=max(ex_pertime[i][j][leftside:rightside])
            temp5.append(temp_max_ex)
            temp6.append(temp_max_ey)
            temp7.append(leftside)#range_of_search)
        max_ex.append(temp5)
        max_ey.append(temp6)
        rng.append(temp7)
    for i in xrange(len(filevar)):
        avg_ex.append(numpy.mean(max_ex[i]))
        avg_ey.append(numpy.mean(max_ey[i]))
    later=time.clock()
    timecost=later-now
    print "End time=", later
    print "Congratulations, you're", timecost/60.0, "minutes older than when this run started!"
    return avg_ex,avg_ey,max_ex,max_ey

def energy_spectrum(filedir,filedesc,filevar,time_to_max,bins=100,time_is_relative="false"):
    import numpy
    import cPickle
    import time
    now=time.clock()
    print "Start time=",now
    filefirstlet="n"
    p_pertime=[]
    
    hist=[]
    for i in xrange(len(filevar)):
        datfile=filedir+str(filevar[i])+"/energy.dat"
        data_file = open(datfile,'r')
        lines = data_file.readlines()
        data_file.close()
        temp1=[]
        for line in lines:
            temp1.append(float(line.strip().split()[0]))
        total_time=max(temp1)
        p_time=[]
        pcklfile=filedir+filefirstlet+"_"+filedesc+"_"+str(filevar[i])+".pckl"
        print pcklfile
        [filedirectory,filefirstletter,colnum,laststep,stepsize,totalnum,data] = cPickle.load(open(pcklfile))
        if time_is_relative=="false":
            timetomax=int((time_to_max[i]/total_time)*len(data[0]))
        else:
            timetomax=int(time_to_max[i]*len(data[0]))
        temp1=[]
        temp2=[]
        for j in xrange(len(data[0][0])):
            #temp1.append(abs(data[1][0][j]))
            temp2.append(abs(data[1][timetomax][j]))
        #p_time.append(temp1)
        #p_time.append(temp2)
        #p_pertime.append(p_time)
        p_pertime.append(temp2)
    for i in xrange(len(filevar)):
        #temp6=[]
        temp6=numpy.histogram(p_pertime[i],bins)
        #total_num=float(sum(temp6[0]))
        #temp6.append(numpy.histogram(p_pertime[i][0],bins))
        #temp6.append(numpy.histogram(p_pertime[i][1],bins))
        hist.append(temp6)
    later=time.clock()
    timecost=later-now
    print "End time=", later
    print "Congratulations, you're", timecost/60.0, "minutes older than when this run started!"
    return hist

def plot_energy_spectra(hist,titlename="",style='log',normx='true'):
    import matplotlib.pyplot as plt
    import numpy
    x=[]
    y=[]
    total_parts=float(sum(hist[0][:]))
    max_momentum=float(max(hist[1]))
    for i in xrange(len(hist[0])):
        y.append(hist[0][i]/total_parts)
    if normx=='true':
        for i in xrange(len(hist[1])-1):
            x.append(hist[1][i+1]/max_momentum)
    else:
        for i in xrange(len(hist[1])-1):
            x.append(hist[1][i+1])
    
    plt.plot(x,y,'o-',linewidth=2,markersize=3)
    plt.yscale(style)
    plt.xscale(style)
    #plt.title("Energy Spectrum - "+titlename)
    #plt.xlabel("Momentum")
    #plt.ylabel("Fraction of Particles")
    return total_parts
    
def make_phase_diagrams(filedir,filedesc,filevar,actual_a0,when=0.5,xlimit=1.0):
    #ex: a0_from_py("/share/dragon/lauc2/data/pwfa/mass-ratios/","mass",[1,2,5,10])
    #returns [max(a0),average(a0)]
    import cPickle
    import time
    import matplotlib.pyplot as plt
    now=time.clock()
    print "Start time=",now
    filefirstlet1="n"
    filefirstlet2="f"
    particle_x_momentum=[]
    particle_x_position=[]
    field_y_amplitude=[]
    field_x_amplitude=[]
    field_x_position=[]
    
    for i in xrange(len(filevar)):
        pcklfile1=filedir+filefirstlet1+"_"+filedesc+"_"+str(filevar[i])+".pckl"
        pcklfile2=filedir+filefirstlet2+"_"+filedesc+"_"+str(filevar[i])+".pckl"
        
        [filedirectory,filefirstletter,colnum,laststep,stepsize,totalnum,data] = cPickle.load(open(pcklfile1))
        time_to_plot=int(when*len(data[:]))-1
        temp1=data[time_to_plot][0][:]
        temp2=data[time_to_plot][1][:]
        particle_x_position.append(temp1)
        particle_x_momentum.append(temp2)
        max_momentum=(max(temp2))
        
        [filedirectory,filefirstletter,colnum,laststep,stepsize,totalnum,data] = cPickle.load(open(pcklfile2))
        time_to_plot=int(when*len(data[:]))-1
        temp1=data[time_to_plot][0][:]
        temp2=data[time_to_plot][2][:]
        temp3=data[time_to_plot][1][:]
        max_field=max(temp2)
        max_fieldx=max(temp3)
        normalization=max_momentum/max_field
        normalizationx=max_momentum/max_fieldx
        temp4=[]
        temp5=[]
        for j in xrange(len(data[time_to_plot][1][:])):
            temp4.append(data[time_to_plot][2][j]*normalization)
            temp5.append(data[time_to_plot][1][j]*normalizationx)
        field_x_position.append(temp1)
        field_y_amplitude.append(temp4)
        field_x_amplitude.append(temp5)

    for i in xrange(len(field_x_position)): 
        plt.figure(figsize=(14,5))
        plt.subplot(121)
        plt.plot(field_x_position[i],field_y_amplitude[i],'m',linewidth=0.1)
        plt.plot(particle_x_position[i],particle_x_momentum[i],'b.',markersize=0.05)
        plt.legend(["EM Pulse","Electrons"],loc=3)
        plt.title("Phase Diagram -- "+filedesc+" -- "+str(round(actual_a0[i],2)))
        plt.xlabel(r"x $[c$ $\omega_{p0}]$")
        plt.ylabel(r"$p_y [m_e$ $c]$")
        plt.xlim([min(field_x_position[i]),xlimit*max(field_x_position[i])])
        plt.subplot(122)
        plt.plot(field_x_position[i],field_x_amplitude[i],'c',linewidth=0.3)
        plt.plot(particle_x_position[i],particle_x_momentum[i],'b.',markersize=0.05)
        plt.legend([r"$E_x$","Electrons"],loc=3)
        plt.title("Phase Diagram -- "+filedesc+" -- "+str(round(actual_a0[i],2)))
        plt.xlabel(r"x $[c$ $\omega_{p0}]$")
        plt.ylabel(r"$p_y [m_e$ $c]$")
        plt.xlim([min(field_x_position[i]),xlimit*max(field_x_position[i])])

    later=time.clock()
    print "End time=", later
    timecost=later-now
    print "Congratulations, you're", timecost/60.0, "minutes older than when this run started!"

def density(filedir,filedesc,filevar,bins=1000):
    #ex: a0_from_py("/share/dragon/lauc2/data/pwfa/mass-ratios/","mass",[1,2,5,10])
    #returns [max(a0),average(a0)]
    import numpy
    import cPickle
    import time
    now=time.clock()
    print "Start time=",now
    filefirstlet="n"
    n_pertime=[]
    
    hist=[]
    for i in xrange(len(filevar)):
        pcklfile=filedir+filefirstlet+"_"+filedesc+"_"+str(filevar[i])+".pckl"
        print pcklfile
        [filedirectory,filefirstletter,colnum,laststep,stepsize,totalnum,data] = cPickle.load(open(pcklfile))
        n_pertime.append(data[0])
    for i in xrange(len(filevar)):
        temp6=[]
        for j in xrange(len(n_pertime[0])):
            temp6.append(numpy.histogram(n_pertime[i][j],bins))
        hist.append(temp6)
    later=time.clock()
    timecost=later-now
    print "End time=", later
    print "Congratulations, you're", timecost/60.0, "minutes older than when this run started!"
    return hist

def plot_density(hist,titlename="",style='linear'):
    import matplotlib.pyplot as plt
    import numpy
    x=[]
    for i in xrange(len(hist[0][1])-1):
        x.append(hist[0][1][i+1])
    plt.plot(x,hist[1][0],'g--')
    #plt.plot(x,hist[len(hist)/2][0],'g-')
    plt.plot(x,hist[len(hist)-1][0],'b-')
    plt.yscale(style)
    plt.legend(["initial","final"])
    plt.title("Density -- "+titlename)
    plt.xlabel("Position")
    plt.ylabel("Density ("+style+")")
    return

def make_phase_diagrams_for_paper(filedir,filedesc,filevar,fulltitle,subtitle,when=0.5,xlimit=1.0):
    #ex: a0_from_py("/share/dragon/lauc2/data/pwfa/mass-ratios/","mass",[1,2,5,10])
    #returns [max(a0),average(a0)]
    import cPickle
    import time
    import matplotlib.pyplot as plt
    now=time.clock()
    print "Start time=",now
    filefirstlet1="n"
    filefirstlet2="f"
    particle_x_momentum=[]
    particle_x_position=[]
    field_y_amplitude=[]
    field_x_amplitude=[]
    field_x_position=[]
    
    for i in xrange(len(filevar)):
        pcklfile1=filedir+filefirstlet1+"_"+filedesc+"_"+str(filevar[i])+".pckl"
        pcklfile2=filedir+filefirstlet2+"_"+filedesc+"_"+str(filevar[i])+".pckl"
        
        [filedirectory,filefirstletter,colnum,laststep,stepsize,totalnum,data] = cPickle.load(open(pcklfile1))
        time_to_plot=int(when*len(data[0][:]))-1
        temp1=data[0][time_to_plot][:]
        temp2=data[1][time_to_plot][:]
        particle_x_position.append(temp1)
        particle_x_momentum.append(temp2)
        max_momentum=(max(temp2))
        
        [filedirectory,filefirstletter,colnum,laststep,stepsize,totalnum,data] = cPickle.load(open(pcklfile2))
        time_to_plot=int(when*len(data[0][:]))-1
        temp1=data[0][time_to_plot][:]
        temp2=data[2][time_to_plot][:]
        temp3=data[1][time_to_plot][:]
        max_field=max(temp2)
        max_fieldx=max(temp3)
        normalization=max_momentum/max_field
        normalizationx=max_momentum/max_fieldx
        temp4=[]
        temp5=[]
        for j in xrange(len(data[1][time_to_plot][:])):
            temp4.append(data[2][time_to_plot][j]*normalization)
            temp5.append(data[1][time_to_plot][j]*normalizationx)
        field_x_position.append(temp1)
        field_y_amplitude.append(temp4)
        field_x_amplitude.append(temp5)
    
    fig=plt.figure(figsize=(5*len(filevar),5))
    fig.suptitle(fulltitle)
    for i in xrange(len(field_x_position)): 
        ax=fig.add_subplot(1,len(filevar),i+1)
        ax.plot(field_x_position[i],field_x_amplitude[i],'c',linewidth=0.6)
        ax.plot(particle_x_position[i],particle_x_momentum[i],'bo',markersize=0.1)
        ax.set_title(subtitle[i], dpi=300)
        ax.xlim([min(field_x_position[i]),xlimit*max(field_x_position[i])])
    ax1=fig.add_subplot(111)
    ax1.set_xlabel("Position")
    ax1.set_ylabel("Momentum")

    later=time.clock()
    print "End time=", later
    timecost=later-now
    print "Congratulations, you're", timecost/60.0, "minutes older than when this run started!"

# <codecell>


