#!bin/sh
cat <<EOF

set lmargin 5
set rmargin 1
set tmargin 0
#set xrange [0:30]
set term png
EOF


for file in p*.dat
do
pfile=$file
nfile=`echo $file | sed -e s/^p/n/;`
ffile=`echo $file | sed -e s/^p/f/;`
pngfile=`echo $file | sed -e s/dat$/png/;`
movfile=`echo $pngfile | sed -e s/^p/field_lines_/;`

cat << EOF
set size 1,1
set out"$movfile"
set multiplot

set size 0.5,0.45
set origin 0,0.5
set format x""
set bmargin 1
set tmargin 1
#set yrange[-200:1000]
plot"$pfile"u 1:2 w d lt 1,\
"$nfile"u 1:2 w d lt 3

set size 0.5,0.45
set origin 0,0
set format x
set bmargin 1
set tmargin 1
#set yrange[-0.5:0.5]
#plot"$ffile"u 1:2 w l lt 6
#set yrange[0:1]
plot "$ffile" u 1:11 w l

set size 0.5,0.45
set origin 0.5,0
set format x
set bmargin 1
set tmargin 1
#set yrange[-15000:15000]
plot"$ffile" u 1:3 w l lt 6

set size 0.5,0.45
set origin 0.5,0.5
set format x
set bmargin 1
set tmargin 1
#set yrange[-200:200]
plot "$pfile" u 1:3 w d lt 1,\
"$nfile" u 1:3 w d lt 3

unset multiplot


EOF

done

echo set term x11

#set nomultiplot
